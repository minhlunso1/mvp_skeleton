package com.bravesoft.tavi.data.model.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by minhnguyen on 11/14/17.
 */

public class LoginRequest {
    @SerializedName("email")
    public String email;
    @SerializedName("password")
    public String password;
    @SerializedName("device_token")
    public String deviceToken;
    @SerializedName("os")
    public int os;

    public LoginRequest(String email, String password, String deviceToken, int os) {
        this.email = email;
        this.password = password;
        this.deviceToken = deviceToken;
        this.os = os;
    }
}
