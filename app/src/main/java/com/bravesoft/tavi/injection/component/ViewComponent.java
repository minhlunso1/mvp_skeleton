package com.bravesoft.tavi.injection.component;

import com.bravesoft.tavi.injection.annotation.SingletonScope;
import com.bravesoft.tavi.injection.module.ActivityModule;
import com.bravesoft.tavi.view.activity.BaseActivity;
import com.bravesoft.tavi.view.activity.SampleActivity;

import dagger.Component;

/**
 * Created by Minh on 11/9/2017.
 */

@SingletonScope
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ViewComponent {
    void inject(SampleActivity activity);
}
