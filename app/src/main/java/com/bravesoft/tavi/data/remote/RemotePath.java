package com.bravesoft.tavi.data.remote;

/**
 * Created by minhnguyen on 11/14/17.
 */

public interface RemotePath {
    String API_VERSION = "v1/";
    String LOGIN = API_VERSION + "login";
}
