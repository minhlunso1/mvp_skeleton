package com.bravesoft.tavi.view.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bravesoft.tavi.data.model.request.LoginRequest;
import com.bravesoft.tavi.data.remote.RemoteManager;
import com.bravesoft.tavi.presenter.BasePresenter;
import com.bravesoft.tavi.presenter.SamplePresenter;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by minhnguyen on 11/13/17.
 */

public class SampleActivity extends BaseActivity implements SamplePresenter.ISample {
    SamplePresenter presenter;

    @Inject
    RemoteManager remoteManager;

    @Override
    protected BasePresenter initPresenter() {
        return presenter = new SamplePresenter(this, this);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginDone(String token) {
        Toast.makeText(this, "Login succesfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new RelativeLayout(this));
        ButterKnife.bind(this);
        this.getViewComponent().inject(this);
        presenter.bindComponent(remoteManager);

        LoginRequest request = new LoginRequest("email", "password", Build.DEVICE, 0);
        presenter.login(request);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
