package com.bravesoft.tavi.data.remote;

import com.bravesoft.tavi.data.model.request.LoginRequest;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Minh on 11/11/2017.
 */

@Singleton
public class RemoteManager {
    private final RemoteInterface remoteInterface;
    @Inject
    public RemoteManager(RemoteInterface remoteInterface) {
        this.remoteInterface = remoteInterface;
    }

    public Observable<String> login (LoginRequest request) {
        return remoteInterface.login(request);
    }
}
