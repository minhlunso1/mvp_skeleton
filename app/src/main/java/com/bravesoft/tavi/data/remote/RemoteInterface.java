package com.bravesoft.tavi.data.remote;

import com.bravesoft.tavi.data.model.request.LoginRequest;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;
import rx.Single;

/**
 * Created by Minh on 11/11/2017.
 */

public interface RemoteInterface {
    @POST(RemotePath.LOGIN)
    Observable<String> login(@Body LoginRequest request);
}
