package com.bravesoft.tavi.presenter;

import android.content.Context;

import com.bravesoft.tavi.data.model.request.LoginRequest;
import com.bravesoft.tavi.data.remote.RemoteManager;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by minhnguyen on 11/13/17.
 */

public class SamplePresenter extends BasePresenter {
    private ISample iView;
    private RemoteManager remoteManager;

    public interface ISample extends BaseIView {
        void onLoginDone(String token);
    }

    public SamplePresenter(Context context, ISample iView) {
        super(context);
        this.iView = iView;
    }

    @Override
    public Class getPresenterClass() {
        return presenterClass = this.getClass();
    }

    public void bindComponent(RemoteManager remoteManager) {
        this.remoteManager = remoteManager;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        iView = null;
        remoteManager = null;
    }

    public void login(LoginRequest request) {
        remoteManager.login(request).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .takeUntil(preDestroy())//Important must have
                .subscribe(token -> iView.onLoginDone(token)
                , error -> {
                    iView.onError(error.getMessage());
                    error.printStackTrace();
                });
    }
}
