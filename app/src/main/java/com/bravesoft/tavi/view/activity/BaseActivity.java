package com.bravesoft.tavi.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bravesoft.tavi.R;
import com.bravesoft.tavi.app.TaviApplication;
import com.bravesoft.tavi.injection.component.DaggerViewComponent;
import com.bravesoft.tavi.injection.component.ViewComponent;
import com.bravesoft.tavi.injection.module.ActivityModule;
import com.bravesoft.tavi.presenter.BasePresenter;

import rx.subjects.BehaviorSubject;

/**
 * Created by minhnguyen on 11/13/17.
 */

public abstract class BaseActivity extends AppCompatActivity {
    protected Snackbar snackbar;
    protected boolean canFinishMain;
    protected ProgressDialog pDialog;

    private ViewComponent viewComponent;
    protected BasePresenter basePresenter;

    //Please use this with takeUntil() when apply Rx
    private final BehaviorSubject<BaseActivity> preDestroy = BehaviorSubject.create();
    protected BehaviorSubject<BaseActivity> preDestroy() {
        return preDestroy;
    }

    protected abstract BasePresenter initPresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        canFinishMain = true;
        setupDialog();
        basePresenter = initPresenter();
    }

    @Override
    protected void onDestroy() {
        preDestroy.onNext(this);
        super.onDestroy();
        if (basePresenter != null)
            basePresenter.onDestroy();
    }

    public ViewComponent getViewComponent() {
        if (viewComponent == null) {
            viewComponent = DaggerViewComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(TaviApplication.get(this).getComponent())
                    .build();
        }
        return viewComponent;
    }

    protected void showSnackbar(View view, String message, int length) {
        if (snackbar != null && snackbar.isShown())
            snackbar.dismiss();
        if (message == null || message.isEmpty())
            message = getString(R.string.suggest_check_network);
        snackbar = Snackbar.make(view, message, length);
        snackbar.show();
    }

    protected void toggleSnackbar(View view, boolean toShow, String message) {
        if (toShow) {
            snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        } else {
            if (snackbar != null && snackbar.isShown())
                new Handler().postDelayed(() -> {
                    snackbar.dismiss();
                }, 500);
        }
    }

    protected void setupDialog() {
        pDialog = new ProgressDialog(this, R.style.AppAlertDialogStyle);
        pDialog.setMessage(getString(R.string.loading));
    }

    public void showProgressDialog(String message) {
        if (message != null)
            pDialog.setMessage(message);
        pDialog.show();
    }

    public void hideProgressDialog() {
        pDialog.dismiss();
    }

    public static void startNewTaskWith(Context context, Class activity, Integer intentFlag) {
        Intent intent = new Intent(context, activity);
        if (intentFlag != null)
            intent.addFlags(intentFlag);
        context.startActivity(intent);
    }
}
