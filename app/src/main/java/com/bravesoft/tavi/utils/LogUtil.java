package com.bravesoft.tavi.utils;

import android.util.Log;

import com.bravesoft.tavi.BuildConfig;

public class LogUtil {
    public static final String APPTAG = BuildConfig.APP_NAME;

    public static void e(String str) {
        if (BuildConfig.DEBUG)
            Log.e(APPTAG, str);
    }

    public static void e(String tag, String str) {
        if (BuildConfig.DEBUG)
            Log.e(tag, str);
    }

    public static void d(String str) {
        if (BuildConfig.DEBUG)
            Log.d(APPTAG, str);
    }

    public static void d(String tag, String str) {
        if (BuildConfig.DEBUG)
            Log.d(tag, str);
    }

    public static void i(String str) {
        if (BuildConfig.DEBUG)
            Log.i(APPTAG, str);
    }

    public static void i(String tag, String str) {
        if (BuildConfig.DEBUG)
            Log.i(tag, str);
    }


    public static void w(String str) {
        if (BuildConfig.DEBUG)
            Log.w(APPTAG, str);
    }

    public static void w(String tag, String str) {
        if (BuildConfig.DEBUG)
            Log.w(tag, str);
    }
}
