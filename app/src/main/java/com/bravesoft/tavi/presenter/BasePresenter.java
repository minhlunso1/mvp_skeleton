package com.bravesoft.tavi.presenter;

import android.content.Context;

import com.bravesoft.tavi.utils.LogUtil;

import rx.subjects.BehaviorSubject;

/**
 * Created by minhnguyen on 11/13/17.
 */

public abstract class BasePresenter {
    protected Class presenterClass;

    protected Context context;

    //Please use this with takeUntil() when apply Rx
    private final BehaviorSubject<BasePresenter> preDestroy = BehaviorSubject.create();
    protected BehaviorSubject<BasePresenter> preDestroy() {
        return preDestroy;
    }

    public BasePresenter(Context context) {
        this.context = context;
        getPresenterClass();
    }

    public void onDestroy() {
        preDestroy.onNext(this);

        context = null;
        //set all references to null

        String className = presenterClass == null ? this.getClass().getSimpleName():presenterClass.getSimpleName();
        LogUtil.d(className + " onDestroy");
    }

    public abstract Class getPresenterClass();
}
