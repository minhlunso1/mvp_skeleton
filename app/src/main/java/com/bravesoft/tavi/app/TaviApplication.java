package com.bravesoft.tavi.app;

import android.app.Application;
import android.content.Context;

import com.bravesoft.tavi.data.remote.RemoteManager;
import com.bravesoft.tavi.injection.component.ApplicationComponent;
import com.bravesoft.tavi.injection.component.DaggerApplicationComponent;
import com.bravesoft.tavi.injection.module.ApplicationModule;

import javax.inject.Inject;

/**
 * Created by minhnguyen on 11/13/17.
 */

public class TaviApplication extends Application {
    ApplicationComponent mApplicationComponent;

    @Inject
    RemoteManager remoteManager;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    public static TaviApplication get(Context context) {
        return (TaviApplication) context.getApplicationContext();
    }
}
