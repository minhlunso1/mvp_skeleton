package com.bravesoft.tavi.presenter;

/**
 * Created by minhnguyen on 11/13/17.
 */

public interface BaseIView {
    void onError(String message);
}
