package com.bravesoft.tavi.injection.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.bravesoft.tavi.BuildConfig;
import com.bravesoft.tavi.data.remote.RemoteFactory;
import com.bravesoft.tavi.data.remote.RemoteInterface;
import com.bravesoft.tavi.injection.annotation.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Minh on 11/9/2017.
 */

@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPrefs() {
        return mApplication.getSharedPreferences(BuildConfig.APPLICATION_ID , Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    RemoteInterface provideRemoteInteface() {
        return RemoteFactory.buildRemoteInterface();
    }
}