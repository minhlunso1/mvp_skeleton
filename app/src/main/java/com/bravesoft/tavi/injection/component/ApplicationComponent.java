package com.bravesoft.tavi.injection.component;

import android.app.Application;
import android.content.Context;

import com.bravesoft.tavi.app.TaviApplication;
import com.bravesoft.tavi.data.local.TaviSharedPreferences;
import com.bravesoft.tavi.data.remote.RemoteInterface;
import com.bravesoft.tavi.data.remote.RemoteManager;
import com.bravesoft.tavi.injection.annotation.ApplicationContext;
import com.bravesoft.tavi.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Minh on 11/9/2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(TaviApplication application);

    @ApplicationContext
    Context context();
    Application application();
    TaviSharedPreferences taviSharedPreferences();
    RemoteManager remoteManager();
    RemoteInterface remoteInterface();
}
